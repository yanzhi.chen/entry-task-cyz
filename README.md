# {Entry Task}-陈彦志 开发文档

## 1. 背景及目的 
### 1.1 需求背景
    实现一个移动端活动分享的社交论坛。用户可以根据一些过滤条件来浏览活动、点赞或加入活动，以及查看其他人的资料与发起的活动.
### 1.2 需求目的
    目标用户：发起活动的和查看活动的用户。
    需求的功能：
    * 登录
        * 用户需要登录来访问论坛，未登录用户不允许访问论坛内容
        * 需要支持注册功能
    * 浏览
        * 登陆后，用户可以浏览活动，包括活动列表页和详情页。其中活动列表页需要实现无限滚动行为
        * 访客可以查看活动详情，包括活动名称，描述，活动照片，活动时间和地点， 参加或点赞的访客，以及活动的评论（实现发表评论即可，不需要实现艾特某人回复）
    * 筛选
        * 用户可以在列表页，依据时间和频道对活动进行筛选，频道需要支持多选
    * 互动
        * 支持点赞功能(like)
        * 支持评论功能
        * 支持加入活动功能(going)

## 2. 架构设计 
逻辑架构设计
### 2.1 逻辑架构设计
    整体结构图
    ![image](https://gitlab.com/yanzhi.chen/entry-task-cyz/-/blob/main/Entry_Task_Construction.png)
    逻辑图
    ![image](https://gitlab.com/yanzhi.chen/entry-task-cyz/-/blob/main/Entry_Task_Logic.png)
### 2.2 技术选型
    #### 整体使用react hook+umi+ts的框架进行开发
        * react hook是 React 16.8 的新增特性，它可以让你在不编写 class 的情况下使用 state 以及其他的 React 特性。使用react hook可以更容易的对代码进行复用，代码量也更少。在进行父子组件通信时更加方便。
        * 使用umi框架则是为了更方便的进行项目的开发，一键配置，开箱即用，性能高。
    #### 接口请求使用的是umi-request，它是基于 fetch 封装的开源 http 请求库，在为开发者提供一个统一的 API 调用方式，同时简化使用方式，提供了请求层常用的功能。
## 3. 详细设计 
### 3.1 时序图与流程图
    无
### 3.2 原型与交互设计
    原型和交互如所给文档entry-task-design.zip所示
### 3.3 路由设计
    `routes: [
        { path: '/', redirect: '/login', },
        { path: '/login', component: '@/pages/loginPage/login' },
        { path:'/regist', component:'@/pages/registPage/regist'},
        { path:'/list', component:'@/pages/listPage/list',},
        { path:'/detail',component:'@/pages/detailPage/detail',},
        { path:'/me',component:'@/pages/mePage/me'},
    ]`
    本项目的路由较为简单，按照不同的内容页面对应不同的路有，其中路由的保护是在每次请求的response中进行判断，若登录无效，则返回到login页面，重新登录。
### 3.4 状态管理设计
    本项目未使用redux或者其他的全局状态管理插件，采用react组件之间的通信来进行状态的传递。
### 3.5 核心组件设计
    #### 无限滚动采用虚拟列表+监控滚动条事件触发
    * 虚拟列表使用第三方组件react-list https://github.com/caseywebdev/react-list
    * 无限滚动的实现在onScroll事件处理函数中对e.target.scrollHeight-e.target.scrollTop和e.target.clientHeight的差值进行判断，若发现滚动条接近触底，则再次进行请求，然后将数据续接到之前的列表后
    #### CSS实现loading动画效果
    * 利用border-color属性实现半圆的效果，animation实现转动的效果，fixed属性使其固定在屏幕中间，display属性使其显示或者隐藏。
    #### 详情页轮播图的现实使用第三方Swiper组件
    * https://github.com/nolimits4web/swiper
### 3.6 存储方案设计
    * 本课题的存储方案采用的是sessionStorage，sessionStorage 用于临时保存同一窗口(或标签页)的数据，在关闭窗口或标签页之后将会删除这些数据。
    * 登录后，会在sessionStorage中存入登录时返回的token，然后在每次的请求头部X-BLACKCAT-TOKEN携带该值，向接口请求相应的数据。
### 3.8 异常处理方案设计
    异常处理是在对接口请求时，若发生错误，判断http状态码和返回的data，若判断为登陆失效，则跳转至登陆界面，若是其他的错误，则再次请求。

## Getting Started

npm版本 >= 10.14.2

Install dependencies,

```bash
$ yarn
```

Start the dev server,

```bash
$ yarn start
```
