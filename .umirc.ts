import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { path: '/', redirect: '/login', },
    { path: '/login', component: '@/pages/loginPage/login' },
    { path:'/regist', component:'@/pages/registPage/regist'},
    { path:'/list', component:'@/pages/listPage/list',},
    { path:'/detail',component:'@/pages/detailPage/detail',},
    { path:'/me',component:'@/pages/mePage/me'},
  ],
  fastRefresh: {},
});
