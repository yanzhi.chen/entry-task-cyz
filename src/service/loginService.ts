import {extend} from 'umi-request'

const request = extend({
    prefix:"http://localhost:3000/api/v1",
    timeout: 1000,
    headers: {
        "Content-Type": "application/json"
    },
})
export default request