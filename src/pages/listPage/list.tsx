import { useState, useEffect, useRef } from 'react';
import styles from './list.less';
import 'react-virtualized/styles.css';
import { NavBar } from '../../components/NavBar/NavBar';
import { Activity } from '../../components/Activity/activity';
import { Scroll } from '../../components/Scroll/scroll';
import { Search } from '../../components/Search/search';
import { SubmitButton } from '../../components/SubmitButton/button';
import { Loading } from '../../components/Loading/loading';
import { ReactComponent as Searchicon } from '../../assets/SVGs/search.svg';
import { ReactComponent as Noactivityicon } from '../../assets/SVGs/no-activity.svg';
import { history } from 'umi';
import { extend } from 'umi-request';

type dSelect = {
  text: string;
  before: string;
  after: string;
};

type channelInfo = {
  id: number;
  name: string;
};
export default function ListPage(props) {
  // state
  const [listdata, setListdata] = useState([]);
  const [dateSelect, setDateSelect] = useState({} as dSelect);
  const [channelSelect, setChannelSelect] = useState([] as channelInfo[]);
  const [searchVisible, setSearchVisible] = useState(false);
  const [isSearchResult, setIsSearchResult] = useState(false);
  const [offset, setOffset] = useState(0);
  const [hasmore, setHasmore] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const [allNumber, setAllNumber] = useState(0);
  const containerRef = useRef(null);
  // 组件渲染前
  useEffect(() => {
    const request = extend({
      prefix: 'http://localhost:3000/api/v1',
      timeout: 1000,
      headers: {
        'Content-Type': 'application/json',
        'X-BLACKCAT-TOKEN': sessionStorage.getItem('token') || '',
      },
    });
    setIsLoading(true);
    request
      .get('/events', {
        params: {
          offset: offset,
        },
      })
      .then(function (response) {
        let listdata = response.events.map((value, index: number) => {
          return <Activity key={index + value} data={value}></Activity>;
        });
        setListdata(listdata);
      })
      .catch(function (error) {
        for (let key in error) {
          console.log(key, error[key]);
        }
        if (error.data.error == 'invalid_token') {
          history.push('/login');
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
    return () => {};
  }, []);
  useEffect(() => {
    return () => {};
  }, []);

  // 事件处理函数
  function submitSearch() {
    setOffset(0);
    setIsSearchResult(true);
    setSearchVisible(!setSearchVisible);
    setIsLoading(true);
    let channel = [];
    channelSelect.map((value, index) => {
      channel.push(value.id as never);
    });
    let channelStr: any = channel.toString();
    if (channelStr == '0') {
      channelStr = null;
    }
    const request = extend({
      prefix: 'http://localhost:3000/api/v1',
      timeout: 1000,
      headers: {
        'Content-Type': 'application/json',
        'X-BLACKCAT-TOKEN': sessionStorage.getItem('token') || '',
      },
    });
    request
      .get('/events', {
        params: {
          channels: channelStr,
          after: dateSelect.after,
          before: dateSelect.before,
          offset: 0,
        },
      })
      .then(function (response) {
        setAllNumber(response.total);
        let listdata = response.events.map((value, index) => {
          return <Activity key={index + value} data={value}></Activity>;
        });
        setListdata(listdata);
        setHasmore(response.hasMore);
      })
      .catch(function (error) {
        if (error.data.error == 'invalid_token') {
          history.push('/login');
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  }
  // 监控滚动条
  function scrollHandler(e) {
    // console.log(e.target.scrollHeight-e.target.scrollTop,e.target.clientHeight)
    if (
      e.target.scrollHeight - e.target.scrollTop < e.target.clientHeight + 50 &&
      hasmore
    ) {
      setOffset(offset + 25);
      setIsLoading(true);
      let channel = [];
      channelSelect.map((value, index) => {
        channel.push(value.id as never);
      });
      let channelStr = channel.toString();
      const request = extend({
        prefix: 'http://localhost:3000/api/v1',
        timeout: 1000,
        headers: {
          'Content-Type': 'application/json',
          'X-BLACKCAT-TOKEN': sessionStorage.getItem('token') || '',
        },
      });
      request
        .get('/events', {
          params: {
            channels: channelStr,
            after: dateSelect.after,
            before: dateSelect.before,
            offset: offset + 25,
            limit: 25,
          },
        })
        .then(function (response) {
          let newListdata = response.events.map((value: any, index) => {
            return <Activity key={index} data={value}></Activity>;
          });
          listdata.push(newListdata as never);
          setListdata(listdata);
          setHasmore(response.hasMore);
        })
        .catch(function (error) {
          if (error.data.error == 'invalid_token') {
            history.push('/login');
          }
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  }
  // 时间格式转换
  function formate(timetamp) {
    if (timetamp == '') {
      return 'All time';
    }
    var date = new Date(parseInt(timetamp));
    var year = date.getFullYear();
    var mon = date.getMonth() + 1;
    var day = date.getDate();
    return mon + '/' + day;
  }
  // 通道选择的请求值
  function formateChannel() {
    let result = '';
    let array: string[] = [];
    channelSelect.map((value, index) => {
      array.push(value.name);
    });
    return array.toString();
  }
  return (
    <div className={styles.listContainer} onScroll={scrollHandler}>
      {/* Loading标志 */}
      <Loading isVisible={isLoading} />
      {/* 筛选界面 */}
      <div
        className={styles.searchContainer}
        style={
          searchVisible
            ? { display: 'block', height: `${window.screen.height}px` }
            : { display: 'none' }
        }
      >
        {/* 筛选date和channel */}
        <Search
          setChannelSelect={setChannelSelect}
          setDateSelect={setDateSelect}
        ></Search>
        {/* Button的样式 */}
        <div style={{ width: '100%' }}>
          {(channelSelect.length == 0 && dateSelect.text == undefined) || '' ? (
            <SubmitButton
              // smallinfo={`${selection.channel} activities from ${selection.date.after} to ${selection.date.before}`}
              textColor="#66666"
              title={
                <div className={styles.buttonText}>
                  <Searchicon
                    fill="#666666"
                    className={styles.icon}
                    width={15}
                    height={15}
                  />
                  <span>SEARCH</span>
                </div>
              }
              color="#BABABA"
              clickHandler={() => submitSearch()}
            ></SubmitButton>
          ) : (
            <SubmitButton
              // smallinfo={`${formateChannel()} activities from ${formate(dateSelect.after)} to ${formate(dateSelect.before)}`}
              textColor="#453257"
              title={
                <div className={styles.buttonText}>
                  <Searchicon
                    fill="#453257"
                    className={styles.icon}
                    width={15}
                    height={15}
                  />
                  <span>SEARCH</span>
                </div>
              }
              color="#D5EF7F"
              clickHandler={() => submitSearch()}
            ></SubmitButton>
          )}
        </div>
      </div>
      <div
        className={styles.activityContainer}
        style={
          searchVisible
            ? { transform: 'translateX(240px)', overflow: 'hidden' }
            : {}
        }
      >
        {/* 顶部状体栏 */}
        <NavBar
          leftIcon="search"
          search={setSearchVisible}
          isSearch={searchVisible}
        ></NavBar>

        <div className={styles.margintop} ref={containerRef}>
          <div
            className={styles.screeningResult}
            style={isSearchResult ? { display: 'block' } : { display: 'none' }}
          >
            {/* 筛选后的顶部结果 */}
            <div className={styles.screeningResultTop}>
              <div className={styles.screeningResultNumber}>
                {allNumber} Results
              </div>
              <div className={styles.clearSearch}>CLEAR SEARCH</div>
            </div>
            <div className={styles.screeningResultBottom}>
              {`${formateChannel()} activities from ${formate(
                dateSelect.after,
              )} to ${formate(dateSelect.before)}`}
            </div>
          </div>
          {/* 活动列表 */}
          <div className="scrollContainer">
            {listdata.length == 0 ? (
              <div className={styles.noActivityContainer}>
                <Noactivityicon width={60} height={60} fill="#D3C1E5" />
                <div className={styles.noActivityText}>No activity found</div>
              </div>
            ) : (
              <Scroll listdata={listdata}></Scroll>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
