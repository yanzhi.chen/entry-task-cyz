import { useEffect, useState } from 'react';
import styles from './me.less';
import { extend } from 'umi-request';
import { NavBar } from '../../components/NavBar/NavBar';
import { history } from 'umi';
import { Activity } from '../../components/Activity/activity';
import { Scroll } from '../../components/Scroll/scroll';
import { ReactComponent as Emailicon } from '../../assets/SVGs/email.svg';
import { ReactComponent as Likeicon } from '../../assets/SVGs/like.svg';
import { ReactComponent as Likeoutlineicon } from '../../assets/SVGs/like-outline.svg';
import { ReactComponent as Checkicon } from '../../assets/SVGs/check.svg';
import { ReactComponent as Checkoutlineicon } from '../../assets/SVGs/check-outline.svg';
import { ReactComponent as Pasticon } from '../../assets/SVGs/past.svg';
import { ReactComponent as Pastoutlineicon } from '../../assets/SVGs/Past-outline.svg';
import { ReactComponent as Noactivityicon } from '../../assets/SVGs/no-activity.svg';

export default function MePage(props) {
  const [userInfo, setUserInfo] = useState({} as any);
  const [activeIndex, setActiveIndex] = useState(0);
  const [infoLikesData, setInfoLikesData] = useState([] as any);
  const [infoGoingData, setInfoGoingData] = useState([] as any);
  const [infoPastData, setInfoPastData] = useState([] as any);
  useEffect(() => {
    const request = extend({
      prefix: 'http://localhost:3000/api/v1',
      timeout: 1000,
      headers: {
        'Content-Type': 'application/json',
        'X-BLACKCAT-TOKEN': sessionStorage.getItem('token') || '',
      },
    });
    request
      .get('/user')
      .then((response) => {
        setUserInfo(response);
      })
      .catch((error) => {
        if (error.data.error == 'invalid_token') {
          history.push('/login');
        }
      });
    request
      .get('/user/events', {
        params: {
          type: 'liked',
        },
      })
      .then((response) => {
        setInfoLikesData(response.events);
      })
      .catch((error) => {
        if (error.data.error == 'invalid_token') {
          history.push('/login');
        }
      });
    request
      .get('/user/events', {
        params: {
          type: 'going',
        },
      })
      .then((response) => {
        // console.log(response)
        setInfoGoingData(response.events);
      })
      .catch((error) => {
        if (error.data.error == 'invalid_token') {
          history.push('/login');
        }
      });
    request
      .get('/user/events', {
        params: {
          type: 'past',
        },
      })
      .then((response) => {
        // console.log(response)
        setInfoPastData(response.events);
      })
      .catch((error) => {
        if (error.data.error == 'invalid_token') {
          history.push('/login');
        }
      });
    return () => {};
  }, []);

  function leftClickHandler() {
    history.push('/list');
  }
  function selectBarCLickHandler(index) {
    setActiveIndex(index);
  }
  // tab切换显示
  function userBottom() {
    let likesListData = infoLikesData.map((value, index) => {
      return <Activity data={value} key={index} />;
    });
    let goingsListData = infoGoingData.map((value, index) => {
      return <Activity data={value} key={index} />;
    });
    let pastListData = infoPastData.map((value, index) => {
      return <Activity data={value} key={index} />;
    });
    let noLength = (
      <div className={styles.noLength}>
        <Noactivityicon width={60} height={60} fill="#D3C1E5" />
        <p>No activity found</p>
      </div>
    );
    if (activeIndex == 0) {
      return likesListData.length != 0 ? (
        <div>
          <Scroll listdata={likesListData} />
        </div>
      ) : (
        noLength
      );
    } else if (activeIndex == 1) {
      return goingsListData.length != 0 ? (
        <div>
          <Scroll listdata={goingsListData} />
        </div>
      ) : (
        noLength
      );
    } else {
      return pastListData.length != 0 ? (
        <div>
          <Scroll listdata={pastListData} />
        </div>
      ) : (
        noLength
      );
    }
  }

  return (
    <div className={styles.userContainer}>
      <NavBar leftIcon="home" search={leftClickHandler}></NavBar>
      <div className={styles.userTop}>
        <div className={styles.avatar}>
          <img
            src="https://coding.net/static/fruit_avatar/Fruit-19.png"
            alt=""
            width="72"
            height="72"
          />
        </div>
        <div className={styles.username}>{userInfo.username}</div>
        <div className={styles.email}>
          <Emailicon width={16} height={16} fill="#8560A9" />{' '}
          <span>{userInfo.email}</span>
        </div>
      </div>
      <div className={styles.selectBar}>
        <div onClick={() => selectBarCLickHandler(0)}>
          {activeIndex == 0 ? (
            <div>
              <Likeicon fill="#AECB4F" width={15} height={15} />
              &nbsp;{' '}
              <span style={{ color: '#AECB4F' }}>
                {userInfo.likes_count}&nbsp;Likes
              </span>
            </div>
          ) : (
            <div>
              <Likeoutlineicon fill="#BABABA" width={15} height={15} />
              &nbsp; <span>{userInfo.likes_count}&nbsp;Likes</span>
            </div>
          )}
        </div>
        <div onClick={() => selectBarCLickHandler(1)}>
          {activeIndex == 1 ? (
            <div>
              <Checkicon fill="#AECB4F" width={15} height={15} />
              &nbsp;{' '}
              <span style={{ color: '#AECB4F' }}>
                {userInfo.goings_count}&nbsp;Going
              </span>
            </div>
          ) : (
            <div>
              <Checkoutlineicon fill="#BABABA" width={15} height={15} /> &nbsp;
              <span>{userInfo.goings_count}&nbsp;Going</span>
            </div>
          )}
        </div>
        <div onClick={() => selectBarCLickHandler(2)}>
          {activeIndex == 2 ? (
            <div>
              <Pasticon fill="#AECB4F" width={15} height={15} />
              &nbsp;{' '}
              <span style={{ color: '#AECB4F' }}>
                {userInfo.past_count}&nbsp;Past
              </span>
            </div>
          ) : (
            <div>
              <Pastoutlineicon fill="#BABABA" width={15} height={15} />
              &nbsp; <span>{userInfo.past_count}&nbsp;Past</span>
            </div>
          )}
        </div>
      </div>
      <div className={styles.children}>{userBottom()}</div>
    </div>
  );
}
