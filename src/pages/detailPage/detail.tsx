import styles from './detail.less';
import { NavBar } from '../../components/NavBar/NavBar';
import { Loading } from '../../components/Loading/loading';
import React, { useEffect, useState } from 'react';
// import request from '../../service/getList';
import { extend } from 'umi-request';
import { ReactComponent as Infoicon } from '../../assets/SVGs/info-outline.svg';
import { ReactComponent as Peopleicon } from '../../assets/SVGs/people.svg';
import { ReactComponent as Commenticon } from '../../assets/SVGs/comment-outline.svg';
import { ReactComponent as Datefromicon } from '../../assets/SVGs/date-from.svg';
import { ReactComponent as Datetoicon } from '../../assets/SVGs/date-to.svg';
import { ReactComponent as Checkoutlineicon } from '../../assets/SVGs/check-outline.svg';
import { ReactComponent as Likeoutlineicon } from '../../assets/SVGs/like-outline.svg';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.less';
import { CommitsContainer } from '../../components/CommentsContainer/commentsContainer';
import { history } from 'umi';

let detail: any = {
  id: 1,
  name: 'Activity Title Name Make it Longer May Longer than One Line',
  creator_id: 5,
  channel_id: 3,
  begin_time: '2021-07-29T02:15:04.670Z',
  end_time: '2021-07-30T02:15:04.670Z',
  create_time: '2021-07-29T02:15:04.670Z',
  update_time: '2021-07-29T02:15:04.670Z',
  location: 'Marina Bay Sands',
  location_detail: '10 Bayfront Ave, S018956',
  description:
    '[No longer than 300 chars] Vivamus sagittis, diam in lobortis, sapien arcu mattis erat, vel aliquet sem urna et risus. Ut feugiat sapien mi potenti. Maecenas et enim odio. Nullam massa metus, varius quis vehicula sed, pharetra mollis erat. In quis viverra velit. Vivamus placerat, est nec hendrerit varius, enim dui hendrerit magna, ut pulvinar nibh lorem vel lacus. Mauris a orci iaculis, hendrerit eros sed, gravida leo. In dictum mauris vel augue varius there is south north asim.',
  createdAt: '2021-07-29T02:15:04.671Z',
  updatedAt: '2021-07-29T02:15:04.671Z',
  channel: {
    id: 3,
    name: 'Hiring',
    createdAt: '2021-07-29T02:15:04.662Z',
    updatedAt: '2021-07-29T02:15:04.662Z',
  },
  creator: {
    id: 5,
    username: 'user_2',
    password: 'df10ef8509dc176d733d59549e7dbfaf',
    email: 'user_2@example.com',
    salt: 'abc',
    avatar: 'https://coding.net/static/fruit_avatar/Fruit-3.png',
    createdAt: '2021-07-29T02:15:04.664Z',
    updatedAt: '2021-07-29T02:15:04.664Z',
  },
  images: [
    'https://tse2-mm.cn.bing.net/th?id=OIP.w8XC0KPitDfMEeSv9P3GxgHaEt&w=248&h=160&c=7&o=5&dpr=2&pid=1.7',
    'https://tse2-mm.cn.bing.net/th?id=OIP.B7gjATIkLyifGdknxysjVwHaFj&w=222&h=167&c=7&o=5&dpr=2&pid=1.7',
    'https://tse2-mm.cn.bing.net/th?id=OIP.NI9vpiDmGzrQLPKq23e2_wHaFj&w=234&h=173&c=7&o=5&dpr=2&pid=1.7',
    'https://tse2-mm.cn.bing.net/th?id=OIP.rzUYVz0YoOqkmoehDQcKRgHaEo&w=295&h=181&c=7&o=5&dpr=2&pid=1.7',
    'https://tse2-mm.cn.bing.net/th?id=OIP.wTqIPNLDZ96_gPsHc-pplQHaFI&w=228&h=160&c=7&o=5&dpr=2&pid=1.7',
  ],
  likes_count: 11,
  goings_count: 11,
  me_likes: false,
  me_going: false,
};

export default function DetailPage(props) {
  // state
  const [detailData, setdetailData] = useState(detail as any);
  const [activeIndex, setActiveIndex] = useState(0);
  const [participants, setParticipants] = useState({} as any);
  const [likes, setLikes] = useState({} as any);
  const [commentsData, setCommentsData] = useState([] as any);
  const [isLoading, setIsLoading] = useState(false);
  // 组件渲染前
  useEffect(() => {
    setIsLoading(true);
    const request = extend({
      prefix: 'http://localhost:3000/api/v1',
      timeout: 1000,
      headers: {
        'Content-Type': 'application/json',
        'X-BLACKCAT-TOKEN': sessionStorage.getItem('token') || '',
      },
    });
    request
      .get(`/events/${props.location.query.id}`)
      .then(function (response) {
        setdetailData(response.event);
        setTimeout(() => {}, 0);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
    request
      .get(`/events/${props.location.query.id}/participants`)
      .then(function (response) {
        setParticipants(response.users);
      })
      .catch(function (error) {
        console.log(error);
      });
    request
      .get(`/events/${props.location.query.id}/likes`)
      .then(function (response) {
        setLikes(response.users);
      })
      .catch(function (error) {
        console.log(error);
      });
    request
      .get(`/events/${props.location.query.id}/comments`)
      .then(function (response) {
        setCommentsData(response.comments);
      })
      .catch(function (error) {
        console.log(error);
      });
    return () => {};
  }, []);

  // navBar 点击事件处理函数
  function navBarClickHandler() {
    history.push('/list');
  }
  // 时间格式转换
  function formateDate(str) {
    const weekDays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    let time = new Date(str);
    let date = time.getDate();
    let month = weekDays[time.getMonth()];
    let year = time.getFullYear();
    let hour = time.getHours();
    let minute = time.getMinutes();
    let timezone = '';
    if (hour > 12) {
      hour -= 12;
      timezone = ' PM';
    } else {
      timezone = ' AM';
    }
    return {
      yymmdd: `${date} ${month} ${year}`,
      time: (
        <div>
          <span>{`${hour}:${minute}`}</span>
          <span>{timezone}</span>
        </div>
      ),
    };
  }
  // selectBar
  function selectBarCLickHandler(index) {
    setActiveIndex(index);
  }
  return (
    <div className={styles.allContainer} id="top">
      {/* 加载 */}
      <Loading isVisible={isLoading} />
      <NavBar leftIcon="home" search={navBarClickHandler}></NavBar>
      <div className={styles.detailContainer}>
        <div className={styles.topChannel}>
          <span>{detailData.channel.name}</span>
        </div>
        <div className={styles.title}>{detailData.name}</div>
        <div className={styles.creatorInfo}>
          <div className={styles.avatar}>
            <img
              src={detailData.creator.avatar}
              width="36"
              height="36"
              alt=""
            />
          </div>
          <div className={styles.info}>
            <p className={styles.infoName}>{detailData.creator.username}</p>
            <p className={styles.publishTime}>
              Published{' '}
              {Math.floor(
                (new Date().valueOf() -
                  new Date(detailData.creator.createdAt).valueOf()) /
                  86400000,
              )}{' '}
              days ago
            </p>
          </div>
        </div>
        {/* selectBar */}
        <div className={styles.selectBar}>
          <div
            className={activeIndex == 0 ? styles.active : ''}
            onClick={() => selectBarCLickHandler(0)}
          >
            <Infoicon
              fill={activeIndex == 0 ? '#AECB4F' : '#8C8C8C'}
              width={15}
              height={15}
            />{' '}
            <span>
              <a style={{ color: 'inherit' }} href="#top">
                Details
              </a>
            </span>
          </div>
          <div
            className={activeIndex == 1 ? styles.active : ''}
            onClick={() => selectBarCLickHandler(1)}
          >
            <Peopleicon
              fill={activeIndex == 1 ? '#AECB4F' : '#8C8C8C'}
              width={15}
              height={15}
            />{' '}
            <span>
              <a style={{ color: 'inherit' }} href="#participants">
                Participants
              </a>
            </span>
          </div>
          <div
            className={activeIndex == 2 ? styles.active : ''}
            onClick={() => selectBarCLickHandler(2)}
          >
            <Commenticon
              fill={activeIndex == 2 ? '#AECB4F' : '#8C8C8C'}
              width={15}
              height={15}
            />{' '}
            <span>
              <a style={{ color: 'inherit' }} href="#comments">
                Comments
              </a>
            </span>
          </div>
        </div>
        {/* 轮播图 */}
        {detailData.images.length == 0 ? null : (
          <div className={styles.carousel}>
            <Swiper
              spaceBetween={50}
              slidesPerView={1.75}
              onSlideChange={() => null}
              onSwiper={(swiper) => null}
            >
              {detailData.images.map((value, index) => {
                return (
                  <SwiperSlide key={index} style={{ paddingRight: '-20px' }}>
                    <div className={styles.slide}>
                      <img src={value} width="180" height="100" alt="" />
                    </div>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </div>
        )}
        <div className={styles.description}>{detailData.description}</div>
        <div className={styles.when}>
          <p>When</p>
          <div className={styles.time}>
            <div className={styles.timeLeft}>
              <div className={styles.timeLeftTop}>
                <Datefromicon fill="#D5EF7F" width={12} height={12} />
                <span>{formateDate(detailData.begin_time).yymmdd}</span>
              </div>
              <div className={styles.timeLeftBottom}>
                <div>{formateDate(detailData.begin_time).time}</div>
              </div>
            </div>
            <div className={styles.timeRight}>
              <div className={styles.timeRightTop}>
                <Datetoicon width={12} height={12} fill="#D5EF7F" />
                <span>{formateDate(detailData.end_time).yymmdd}</span>
              </div>
              <div className={styles.timeRightBottom}>
                <div>{formateDate(detailData.end_time).time}</div>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.where}>
          <p>Where</p>
          <p>{detailData.location}</p>
          <p>{detailData.location_detail}</p>
          <div>
            <img
              src={require('../../assets/Imgs/gmap.png')}
              width="288"
              height="88"
              alt=""
            />
          </div>
        </div>
        {/* going */}
        <div className={styles.going} id="participants">
          <div className={styles.goingLeft}>
            <Checkoutlineicon fill="#AC8EC9" width={10} height={10} />{' '}
            <span>{participants.length} going</span>
          </div>
          <div className={styles.goingRight}>
            {participants.length == undefined || 0
              ? ''
              : participants.map((value, index) => {
                  return (
                    <div key={index}>
                      <img src={value.avatar} width="20" height="20" alt="" />
                    </div>
                  );
                })}
          </div>
        </div>
        {/* likes */}
        <div className={styles.likes}>
          <div className={styles.likesLeft}>
            <Likeoutlineicon fill="#AC8EC9" width={10} height={10} />{' '}
            <span>{likes.length} likes</span>
          </div>
          <div className={styles.likesRight}>
            {likes.length == undefined || 0
              ? ''
              : likes.map((value, index) => {
                  return (
                    <div key={index}>
                      <img src={value.avatar} width="20" height="20" alt="" />
                    </div>
                  );
                })}
          </div>
        </div>
        {/* 评论区 */}
        <div className={styles.commentsContainer} id="comments">
          <CommitsContainer
            setCommentsData={setCommentsData}
            setdetailData={setdetailData}
            activityId={props.location.query.id}
            commentsData={commentsData}
            me_likes={detailData.me_likes}
            me_going={detailData.me_going}
          ></CommitsContainer>
        </div>
      </div>
    </div>
  );
}
