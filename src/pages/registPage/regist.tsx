import styles from './regist.less';
import { ReactComponent as Logocat } from '../../assets/SVGs/logo-cat.svg';
import React, { useState } from 'react';
import { LoginInput } from '../../components/LoginInput/input';
import { SubmitButton } from '../../components/SubmitButton/button';
import { Link } from 'umi';
import request from '../../service/loginService';
import { history } from 'umi';
const loginText = {
  english: [
    'FIND THE MOST LOVED ACTIVITIES',
    'BLACK CAT',
    '请输入账号',
    '请输入密码',
    'LOG IN',
    '请输入邮箱',
    '注册',
  ],
  chinese: [
    '找寻最喜欢的活动',
    '黑猫',
    'Account',
    'Password',
    '登录',
    'email',
    'REGIST',
  ],
};

export default function LoginPage() {
  const [language, setLanguage] = useState('english');
  const [text, setText] = useState(loginText.english);
  const [account, setAccount] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  function changeLanguege() {
    if (language == 'english') {
      setLanguage('chinese');
      setText(loginText.chinese);
    } else {
      setLanguage('english');
      setText(loginText.english);
    }
  }
  function registSubmit() {
    request
      .post('/join', {
        data: {
          username: account,
          password: password,
          email: email,
          avatar: 'https://coding.net/static/fruit_avatar/Fruit-19.png',
        },
      })
      .then(function (response) {
        window.sessionStorage.setItem('token', response.token);
        history.push('/list');
      })
      .catch(function (error) {
        // console.log(error)
        for (let key in error) {
          console.log(key, error[key]);
        }
      });
  }
  return (
    <div className={styles.allContainer}>
      <div className={styles.violet}>
        <p onClick={changeLanguege} className={styles.languageSelect}>
          {language}
        </p>
        <div className={styles.container}>
          <p className={styles.info1}>{text[0]}</p>
          <p className={styles.info2}>{text[1]}</p>
          <div className={styles.logoOuter}>
            <div className={styles.logoInner}>
              <Logocat
                width={45}
                height={45}
                fill="#D5EF7F"
                className={styles.logo}
              />
            </div>
          </div>
          <div className={styles.iform}>
            {/* 表单 */}
            <LoginInput
              placeholder={
                language == 'english'
                  ? loginText.chinese[2]
                  : loginText.english[2]
              }
              leftIcon="user"
              setValue={setAccount}
              type="text"
            ></LoginInput>
            <LoginInput
              placeholder={
                language == 'english'
                  ? loginText.chinese[3]
                  : loginText.english[3]
              }
              leftIcon="password"
              setValue={setPassword}
              type="password"
            ></LoginInput>
            <LoginInput
              placeholder={
                language == 'english'
                  ? loginText.chinese[5]
                  : loginText.english[5]
              }
              leftIcon="email"
              setValue={setEmail}
              type="email"
            ></LoginInput>
          </div>
          <Link to="/login" style={{ color: 'white' }}>
            {language == 'english'
              ? loginText.english[4]
              : loginText.chinese[4]}
          </Link>
          <SubmitButton
            textColor="#453257"
            title={
              language == 'english'
                ? loginText.chinese[6]
                : loginText.english[6]
            }
            color="#D5EF7F"
            clickHandler={registSubmit}
          ></SubmitButton>
        </div>
      </div>
    </div>
  );
}
