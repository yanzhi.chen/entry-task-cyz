import styles from './button.less';
import React from 'react';
interface BProps{
    title:any;
    color:string;
    smallinfo?:string;
    clickHandler(...arg:any[]):any;
    textColor:string;
}

export const SubmitButton:React.FC<BProps> = ({title,color,smallinfo,textColor,clickHandler})=>{
    function small(){
        if(smallinfo == '' || undefined || null){
            return null
        }else{
            return (
                <p className={styles.smallinfo}>{smallinfo}</p>
            )
        }
    }
    return (
        <div className={styles.container} onClick={clickHandler} style={{backgroundColor:`${color}`}}>
            <div>
                <div style={{backgroundColor:`${color}`,color:`${textColor}`,textAlign:"center"}}>{title}</div>
                {small()}
            </div>
        </div>
    )
}