import styles from './loading.less';

interface LProps{
    isVisible:boolean;
}

export const Loading:React.FC<LProps> = ({isVisible})=>{
    return (
        <div className={styles.loading} style={{display:`${isVisible?"block":"none"}`}}></div>
    )
}