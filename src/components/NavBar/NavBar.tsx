import styles from './NavBar.less';
import React from 'react';
import {ReactComponent as Logocat} from '../../assets/SVGs/logo-cat.svg';
import {ReactComponent as Searchicon} from '../../assets/SVGs/search.svg';
import {ReactComponent as Homeicon} from '../../assets/SVGs/home.svg';
import {ReactComponent as Usericon} from '../../assets/SVGs/user.svg';
import {history} from 'umi';
interface NProps{
    leftIcon:string;
    search(...any:any[]):void;
    isSearch?:boolean;
}

export const NavBar:React.FC<NProps> = ({leftIcon,search,isSearch})=>{
    function goToMe(){
        history.push('/me')
    }
    return (
        <div className={styles.navbarContainer}>
            <div className={styles.left} onClick={()=>{search(!isSearch)}}>
                {leftIcon == 'search'? <Searchicon width={24} height={24} fill="#453257" /> :
                <Homeicon width={24} height={24} fill="#453257" />}
            </div>
            <div className={styles.middle}>
                <Logocat width={20.9} height={24} fill="#D5EF7F" />
            </div>
            <div className={styles.right} onClick={goToMe}>
                <img src="https://coding.net/static/fruit_avatar/Fruit-19.png" alt="" height="24" width="24" />
            </div>
        </div>
    )
}