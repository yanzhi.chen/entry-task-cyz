import React from 'react';
import ReactList from 'react-list';

interface SProps{
    listdata:any[];
}

export const Scroll:React.FC<SProps> = ({listdata})=>{
    function renderItem(index, key) {
        return <div key={key}>{listdata[index]}</div>;
      }
    return (
        <div>
            <ReactList
            itemRenderer={renderItem}
            length={listdata.length}
            type='variable'
          />
        </div>
    )
}