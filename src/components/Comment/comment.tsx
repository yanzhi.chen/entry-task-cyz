import styles from './comment.less';
import { ReactComponent as Replyicon } from '../../assets/SVGs/reply.svg';
import { useState } from 'react';

interface CProps {
  commitData: any;
  setCommentValue(str: string): void;
}

export const Commit: React.FC<CProps> = ({ commitData, setCommentValue }) => {
  const [startTime, setStartTime] = useState(new Date().valueOf());
  const [endTime, setEndTime] = useState(new Date().valueOf());
  function formate(dateStr) {
    const createTime = new Date(dateStr).valueOf();
    const now = new Date().valueOf();
    let result = Math.floor((now - createTime) / 86400000);
    if (result >= 24) {
      result = Math.floor(result / 24);
      return `${result} days ago`;
    } else {
      return `${result} hours ago`;
    }
  }

  function touchStartHandler() {
    console.log('in');
    setStartTime(new Date().valueOf());
  }

  function touchEndHandler() {
    console.log('out');
    setEndTime(new Date().valueOf());
    console.log(endTime - startTime);
    if (endTime - startTime < -1000) {
      console.log('@');
      setCommentValue(`@${commitData.user.username}`);
    }
  }
  return (
    <div className={styles.commentContainer}>
      <div
        className={styles.left}
        onMouseEnter={touchStartHandler}
        onMouseLeave={touchEndHandler}
      >
        <img src={commitData.user.avatar} width="32" height="32" alt="" />
      </div>
      <div className={styles.right}>
        <div className={styles.rightTop}>
          <div>
            <p>{commitData.user.username}</p>
            <p>{formate(commitData.createdAt)}</p>
          </div>
          <div>
            <Replyicon fill="#D5EF7F" width={13} height={13} />
          </div>
        </div>
        <div className={styles.rightBottom}>{commitData.comment}</div>
      </div>
    </div>
  );
};
