import React from 'react';
import styles from './activity.less';
import { ReactComponent as Timeicon } from '../../assets/SVGs/time.svg';
import { ReactComponent as Checkicon } from '../../assets/SVGs/check.svg';
import { ReactComponent as Checkoutlineicon } from '../../assets/SVGs/check-outline.svg';
import { ReactComponent as Likeicon } from '../../assets/SVGs/like.svg';
import { ReactComponent as Likeoutlineicon } from '../../assets/SVGs/like-outline.svg';
import { history } from 'umi';

interface AProps {
  data: any;
}

export const Activity: React.FC<AProps> = ({ data }) => {
  function isLike() {
    if (data.me_likes) {
      return (
        <span className={styles.likes}>
          <Likeicon fill="#FF5C5C" width={10} height={10} />{' '}
          <span>&nbsp;I like it</span>
        </span>
      );
    } else {
      return (
        <span className={styles.likeoutline}>
          <Likeoutlineicon fill="#AC8EC9" width={10} height={10} />{' '}
          <span>{data.likes_count} Likes</span>
        </span>
      );
    }
  }
  function isCheck() {
    if (data.me_going) {
      return (
        <span className={styles.check}>
          <Checkicon fill="#AECB4F" width={10} height={10} />{' '}
          <span>&nbsp;I am going!</span>
        </span>
      );
    } else {
      return (
        <span className={styles.checkoutline}>
          <Checkoutlineicon fill="#AC8EC9" width={10} height={10} />{' '}
          <span>{data.goings_count} Going</span>{' '}
        </span>
      );
    }
  }
  function clickHandler(id) {
    history.push({
      pathname: '/detail',
      query: {
        id: id,
      },
    });
  }
  function formate(str) {
    const months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    const time = new Date(str);
    const month = months[time.getMonth()];
    const date = time.getDate().toString();
    const hour = time.getHours().toString();
    const minutes = time.getMinutes().toString();
    const year = time.getFullYear().toString();
    return `${date} ${month} ${year} ${hour}:${minutes}`;
  }
  return (
    <div className={styles.out} onClick={() => clickHandler(data.id)}>
      <div className={styles.activityContainer}>
        <div className={styles.top}>
          <div className={styles.avatar}>
            <img src={data.creator.avatar} alt="" width="20" height="20" />
            <span>{data.creator.username}</span>
          </div>
          <div className={styles.channelName}>Channel {data.channel.name}</div>
        </div>
        <div className={styles.middle}>
          <div className={styles.middleLeft}>
            <div className={styles.title}>{data.name}</div>
            <div className={styles.time}>
              <Timeicon
                width={15}
                height={15}
                fill="#8560A9"
                style={{ marginRight: '4px' }}
              />
              <span>
                {formate(data.begin_time)}-{formate(data.end_time)}
              </span>
            </div>
            <div className={styles.textarea}>
              <p className={styles.description}>{data.description}</p>
            </div>
          </div>
          {data.images == undefined ? null : (
            <div
              className={styles.middleRight}
              style={{
                flex: `${data.images.length == 0 || undefined ? '0' : '1'}`,
              }}
            >
              <div
                className={styles.dataImg}
                style={{
                  display: `${data.images.length == 0 ? 'none' : 'inherit'}`,
                }}
              >
                <img src={data.images[0]} alt="" width="64" height="64" />
              </div>
            </div>
          )}
        </div>
        <div className={styles.bottom}>
          {isCheck()} {isLike()}
        </div>
      </div>
    </div>
  );
};
