import styles from './search.less';
import {DateSearch} from './dateSearch';
import {ChannelSearch} from './channelSearch';
import {SubmitButton} from '../SubmitButton/button';
import {ReactComponent as Searchicon} from '../../assets/SVGs/search.svg';
import { useState } from 'react';

interface SProps{
    setDateSelect(...arg:any[]):any;
    setChannelSelect(...arg:any[]):any;
}

export const Search:React.FC<SProps> = ({setChannelSelect,setDateSelect})=>{
    // const [dateSelect, setDateSelect] = useState({})
    // const [channelSelect, setChannelSelect] = useState("")

    return (
        <div className={styles.searchContainer} >
            <div>
                <div>
                    <DateSearch setDate={setDateSelect}></DateSearch>
                </div>
                <div>
                    <ChannelSearch setChannel={setChannelSelect}></ChannelSearch>
                </div>
            </div>
            
        </div>
    )
}