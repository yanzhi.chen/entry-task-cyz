import { useEffect, useState } from 'react';
import styles from './channelSearch.less';
import {extend} from 'umi-request'

interface CProps{
    setChannel(...args:any[]):any
}

export const ChannelSearch:React.FC<CProps> = ({setChannel})=>{

    const [channelsData, setChannelsData] = useState([])
    const [activeIndexs, setActiveIndexs] = useState([] as number[])

    function clickHandler(value,index){
        let newActiveIndexs = activeIndexs
        let where = activeIndexs.indexOf(index)
        if(where != -1){
            newActiveIndexs = activeIndexs
            newActiveIndexs.splice(where,1)
        }else{
            newActiveIndexs.push(index)
        }
        newActiveIndexs = Array.from(new Set(newActiveIndexs))
        setActiveIndexs(newActiveIndexs)
        let array = []
        let channel = newActiveIndexs.map((value)=>{
            array.push(channelsData[value])
        })
        setChannel(array)
    }
    useEffect(() => {
        const request = extend({
            prefix:"http://localhost:3000/api/v1",
            timeout: 1000,
            headers: {
                "Content-Type": "application/json",
                "X-BLACKCAT-TOKEN":sessionStorage.getItem('token') || ""
            } 
        })
        request.get('/channels')
        .then((response)=>{
            const data = response.channels
            data.unshift({
                createdAt: "",
                id: 0,
                name: "All",
                updatedAt: ""
            })
            setChannelsData(data)
        })
        return () => {
        }
    }, [])
    return (
        <div className={styles.channelSearchContainer}>
            <p>CHANNEL</p>
            <ul>
                {channelsData.map((value:any,index)=>{
                    return (
                        <li className={activeIndexs.indexOf(index) != -1?styles.active:""} onClick={()=>{clickHandler(value,index)}} key={index}>{value.name}</li>
                    )
                })}
            </ul>
        </div>
    )
}