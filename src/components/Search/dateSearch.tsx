import React, { MouseEventHandler, useState } from 'react';
import styles from './dateSearch.less';
import { ReactComponent as Datefromicon } from '../../assets/SVGs/date-from.svg';
import { ReactComponent as Datetoicon } from '../../assets/SVGs/date-to.svg';
import { DatePicker, Space } from 'antd';
interface DProps {
  setDate(...arg: any[]): any;
}

const { RangePicker } = DatePicker;
export const DateSearch: React.FC<DProps> = ({ setDate }) => {
  const [isLater, setIsLater] = useState(false);
  const [laterBegin, setLaterBegin] = useState(0);
  const [laterEnd, setLaterEnd] = useState(0);
  const [time, setTime] = useState([
    {
      text: 'ANYTIME',
      before: '',
      after: '',
    },
    {
      text: 'TODAY',
      before: new Date().valueOf(),
      after: new Date().valueOf() - 86400000,
    },
    {
      text: 'TOMORROW',
      before: new Date().valueOf() + 86400000,
      after: new Date().valueOf(),
    },
    {
      text: 'THIS WEEK',
      before: new Date().valueOf() + 604800000,
      after: new Date().valueOf(),
    },
    {
      text: 'THIS MONTH',
      before: new Date().valueOf() + 2592000000,
      after: new Date().valueOf(),
    },
  ]);

  function updateData() {
    const now = new Date();
    setTime([
      {
        text: 'ANYTIME',
        before: '',
        after: '',
      },
      {
        text: 'TODAY',
        before: new Date().valueOf(),
        after: new Date().setHours(0, 0, 0, 0).valueOf(),
      },
      {
        text: 'TOMORROW',
        before: new Date().setDate(now.getDate() + 1).valueOf(),
        after: new Date().valueOf(),
      },
      {
        text: 'THIS WEEK',
        before: new Date().setDate(now.getDate() + 7 - now.getDay()).valueOf(),
        after: new Date().valueOf(),
      },
      {
        text: 'THIS MONTH',
        before: new Date().setDate(30).valueOf(),
        after: new Date().valueOf(),
      },
    ]);
  }

  const [activeNumber, setActiveNumber] = useState(-1);

  function clickHandler(value, index) {
    updateData();
    setActiveNumber(index);
    // console.log(value)
    setDate(value);
    setIsLater(false);
  }
  function clickLater(value) {
    setActiveNumber(-2);
    setIsLater(true);
  }
  function laterBeginTime(date, dateString) {
    // // console.log(date,dateString)
    // console.log(new Date(dateString).valueOf(),new Date(dateString))
    setLaterBegin(new Date(dateString).valueOf());
    setDate({
      text: 'Select Date',
      before: laterEnd,
      after: laterBegin,
    });
  }
  function laterEndTime(date, dateString) {
    // console.log(new Date(dateString).valueOf(),new Date(dateString))
    setLaterEnd(new Date(dateString).valueOf());
    // console.log({
    //     text:'Select Date',
    //     before:laterEnd,
    //     after:laterBegin
    // })
    setDate({
      text: 'Select Date',
      before: laterEnd,
      after: laterBegin,
    });
  }
  return (
    <div className={styles.dateSearchContainer}>
      <p>DATE</p>
      <ul>
        {time.map((value, index) => {
          return (
            <li
              key={index}
              className={activeNumber == index ? styles.active : ''}
              onClick={() => clickHandler(value, index)}
            >
              {value.text}
            </li>
          );
        })}
        <li
          className={activeNumber == -2 ? styles.active : ''}
          onClick={() =>
            clickLater({
              text: 'All',
              before: '',
              after: '',
            })
          }
        >
          LATER
        </li>
      </ul>
      <div
        className={styles.later}
        style={{ display: `${isLater ? 'block' : 'none'}` }}
      >
        <Space direction="horizontal" size={5}>
          <DatePicker
            onChange={laterBeginTime}
            suffixIcon=""
            format="YYYY/MM/DD"
          />
          <DatePicker
            onChange={laterEndTime}
            suffixIcon=""
            format="YYYY/MM/DD"
          />
        </Space>
      </div>
    </div>
  );
};
