import styles from './toast.less';

interface TProps {
  text: string;
  visible: boolean;
}

export const Toast: React.FC<TProps> = ({ text, visible }) => {
  return (
    <div
      className={styles.toast}
      style={{ display: `${visible ? 'block' : 'none'}` }}
    >
      {`I am a toast :) ${text}`}
    </div>
  );
};
