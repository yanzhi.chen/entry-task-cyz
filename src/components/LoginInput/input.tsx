import styles from './input.less';
import React, { ChangeEvent, useState } from 'react';
import { ReactComponent as Usericon } from '../../assets/SVGs/user.svg';
import { ReactComponent as Passwordicon } from '../../assets/SVGs/password.svg';
import { ReactComponent as Emailicon } from '../../assets/SVGs/email.svg';

interface IProps {
  placeholder: string;
  leftIcon?: string;
  setValue(str: string): void;
  type: string;
}

export const LoginInput: React.FC<IProps> = ({
  placeholder,
  leftIcon,
  setValue,
  type,
}) => {
  const [text, setText] = useState('');
  function changeHandler(e: ChangeEvent<{ value: any }>) {
    setText(e.target.value);
    setValue(e.target.value);
  }
  function selectIcon() {
    if (leftIcon == 'password') {
      return <Passwordicon width={13} height={13} fill="#D3C1E5" />;
    } else if (leftIcon == 'email') {
      return <Emailicon width={13} height={13} fill="#D3C1E5" />;
    } else {
      return <Usericon width={13} height={13} fill="#D3C1E5" />;
    }
  }
  return (
    <div className={styles.inputContainer}>
      <div className={styles.icon}>{selectIcon()}</div>
      <input
        className={styles.cInput}
        type={type}
        placeholder={placeholder}
        value={text}
        onChange={changeHandler}
      />
    </div>
  );
};
