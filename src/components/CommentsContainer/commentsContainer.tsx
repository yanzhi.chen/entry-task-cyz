import styles from './commentsContainer.less';
import { Commit } from '../Comment/comment';
import { Toast } from '../Toast/toast';
import { ReactComponent as Commenticon } from '../../assets/SVGs/comment-single.svg';
import { ReactComponent as Likeoutlineicon } from '../../assets/SVGs/like-outline.svg';
import { ReactComponent as Checkoutlineicon } from '../../assets/SVGs/check-outline.svg';
import { ReactComponent as Crossicon } from '../../assets/SVGs/cross.svg';
import { ReactComponent as Sendicon } from '../../assets/SVGs/send.svg';
import { ReactComponent as Likeicon } from '../../assets/SVGs/like.svg';
import { ReactComponent as Checkicon } from '../../assets/SVGs/check.svg';
import { extend } from 'umi-request';
// import commentRequest from '../../service/getList';
import { LoginInput } from '../LoginInput/input';
import { useEffect, useState } from 'react';

interface CProps {
  activityId: any;
  commentsData: any;
  me_likes: boolean;
  me_going: boolean;
  setdetailData(...args: any[]): any;
  setCommentsData(...args: any[]): any;
}

export const CommitsContainer: React.FC<CProps> = ({
  commentsData,
  me_going,
  me_likes,
  activityId,
  setdetailData,
  setCommentsData,
}) => {
  const [userComment, setUserComment] = useState('');
  const [isInput, setIsInput] = useState(false);
  const [toastContent, setToastContent] = useState('successful send');
  const [toastVisible, setToastVisible] = useState(false);
  const [toastTimer, setToastTimer] = useState({} as any);
  function changeHandler(e) {
    setUserComment(e.target.value);
  }
  function changeBar(isInput) {
    setIsInput(isInput);
  }
  function updateDetailData() {
    const request = extend({
      prefix: 'http://localhost:3000/api/v1',
      timeout: 1000,
      headers: {
        'Content-Type': 'application/json',
        'X-BLACKCAT-TOKEN': sessionStorage.getItem('token') || '',
      },
    });
    request
      .get(`/events/${activityId}`)
      .then(function (response) {
        setdetailData(response.event);
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  function isLike() {
    const request = extend({
      prefix: 'http://localhost:3000/api/v1',
      timeout: 1000,
      headers: {
        // "Content-Type": "application/json",
        'X-BLACKCAT-TOKEN': sessionStorage.getItem('token') || '',
      },
    });
    if (me_likes) {
      request.delete(`/events/${activityId}/likes`).then((response) => {
        updateDetailData();
      });
    } else {
      request.post(`/events/${activityId}/likes`).then((response) => {
        updateDetailData();
      });
    }
  }
  function isGoing() {
    const request = extend({
      prefix: 'http://localhost:3000/api/v1',
      timeout: 1000,
      headers: {
        // "Content-Type": "application/json",
        'X-BLACKCAT-TOKEN': sessionStorage.getItem('token') || '',
      },
    });
    if (me_going) {
      request.delete(`/events/${activityId}/participants`).then((response) => {
        updateDetailData();
      });
    } else {
      request.post(`/events/${activityId}/participants`).then((response) => {
        updateDetailData();
      });
    }
  }
  function sendHandler() {
    const request = extend({
      prefix: 'http://localhost:3000/api/v1',
      timeout: 1000,
      headers: {
        'Content-Type': 'application/json',
        'X-BLACKCAT-TOKEN': sessionStorage.getItem('token') || '',
      },
    });
    request
      .post(`/events/${activityId}/comments`, {
        data: {
          comment: userComment,
        },
      })
      .then((response) => {
        setToastContent('successful send');
        setToastVisible(true);
        setToastTimer(
          setTimeout(() => {
            setToastVisible(false);
          }, 2000),
        );
        setUserComment('');
        request
          .get(`/events/${activityId}/comments`)
          .then(function (response) {
            setCommentsData(response.comments);
          })
          .catch(function (error) {
            console.log(error);
          });
      })
      .catch((error) => {
        setToastContent('failed send');
        setToastVisible(true);
        setToastTimer(
          setTimeout(() => {
            setToastVisible(false);
          }, 2000),
        );
      });
  }
  return (
    <div className={styles.commentsContainer}>
      <Toast text={toastContent} visible={toastVisible}></Toast>
      {/* <div className={styles.toast} style={{display:`${toastVisible?"block":"none"}`}}>
                {`I am a toast :) ${toastContent}`}
            </div> */}
      {commentsData.map((value, index) => {
        return (
          <div key={index}>
            <Commit
              setCommentValue={setUserComment}
              commitData={value}
            ></Commit>
          </div>
        );
      })}
      {isInput ? (
        <div className={styles.bar}>
          <div className={styles.inputBarLeft}>
            <Crossicon
              width={15}
              height={15}
              fill="#D5EF7F"
              onClick={() => changeBar(false)}
            />
            <input
              type="text"
              placeholder="Leave your comment here"
              onChange={changeHandler}
              value={userComment}
            />
          </div>
          <div className={styles.inputBarRight} onClick={sendHandler}>
            <Sendicon width={28} height={28} fill="#8560A9" />
          </div>
        </div>
      ) : (
        <div className={styles.bar}>
          <div className={styles.barRight}>
            <div>
              <Commenticon
                width={24}
                height={24}
                onClick={() => changeBar(true)}
              />
            </div>
            <div onClick={isLike}>
              {me_likes ? (
                <Likeicon fill="#D5EF7F" width={24} height={24} />
              ) : (
                <Likeoutlineicon width={24} height={24} />
              )}
            </div>
          </div>
          <div className={styles.barLeft} onClick={isGoing}>
            {me_going ? (
              <div className={styles.check}>
                <Checkicon fill="#8560A9" width={24} height={24} />
                &nbsp;&nbsp;I am going
              </div>
            ) : (
              <div>
                <Checkoutlineicon width={24} height={24} />
                &nbsp;&nbsp;Join
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};
